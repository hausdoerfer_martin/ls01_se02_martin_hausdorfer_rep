import java.util.Scanner;

public class RoemischeZahlen {

	public static void main(String[] args) {

		roemischeZahlen();

	}
	public static void roemischeZahlen() {
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie Ihre gew�nschte R�mische Zahl ein:");
		String roemischeZahl = tastatur.next();

		switch (roemischeZahl) {
		case "I":
			System.out.println("Die entsprechende Dezimalzahl ist: 1!");
			break;
		case "V":
			System.out.println("Die entsprechende Dezimalzahl ist: 5!");
			break;
		case "X":
			System.out.println("Die entsprechende Dezimalzahl ist: 10!");
			break;
		case "L":
			System.out.println("Die entsprechende Dezimalzahl ist: 50!");
			break;
		case "C":
			System.out.println("Die entsprechende Dezimalzahl ist: 100!");
			break;
		case "D":
			System.out.println("Die entsprechende Dezimalzahl ist: 500!");
			break;
		case "M":
			System.out.println("Die entsprechende Dezimalzahl ist: 1000!");
			break;
		default:
			System.out.printf("Fehler!\nBitte geben Sie eine R�mische Zahl von I bis M an!");
			break;

		}
	}

}
