import java.util.Scanner;

public class Quadrat {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie die gew�nschte Gr��e an: ");
		int endwert = tastatur.nextInt();

		for(int hoehe = 1; hoehe <= endwert; hoehe++) {
			for(int laenge = 1; laenge <= endwert; laenge++) {
				if((hoehe == 1) || (laenge == 1) || (hoehe == endwert) || (laenge == endwert)) {
					System.out.print("*");
				}
				else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}

	}

}

