import java.util.Scanner;

public class BMI {

	public static void main(String[] args) {
		bmiRechnung();

	}
	public static void bmiRechnung() {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben sie bitte mit m/w an, ob sie m�nnlich oder weiblich sind: ");
		String geschlecht = tastatur.next();

		System.out.println("Bitte geben sie Ihre Gr��e in Metern an: ");
		double groe�eInM = tastatur.nextDouble();

		System.out.println("Bitte geben sie Ihr Gewicht in kg an: ");
		double gewichtInKg = tastatur.nextDouble();

		double bmi = gewichtInKg / (groe�eInM * groe�eInM);


		if(geschlecht.equals("m") && (bmi < 20)) {
			System.out.printf("Ihre BMI-Klassifikation ist Untergewicht mit einem BMI wert von: %.1f", bmi);
		}

		if(geschlecht.equals("m") && (bmi > 20 && bmi < 25)) {
			System.out.printf("Ihre BMI-Klassifikation ist Normalgewicht mit einem BMI wert von: %.1f", bmi);
		}

		if(geschlecht.equals("m") && (bmi > 25)) {
			System.out.printf("Ihre BMI-Klassifikation ist �bergewicht mit einem BMI wert von: %.1f", bmi);
		}

		if(geschlecht.equals("w") && (bmi < 19)) {
			System.out.printf("Ihre BMI-Klassifikation ist Untergewicht mit einem BMI wert von: %.1f", bmi);
		}

		if(geschlecht.equals("w") && (bmi > 19 && bmi < 24)) {
			System.out.printf("Ihre BMI-Klassifikation ist Normalgewicht mit einem BMI wert von: %.1f", bmi);
		}

		if(geschlecht.equals("w") && (bmi > 24)) {
			System.out.printf("Ihre BMI-Klassifikation ist �bergewicht mit einem BMI wert von: %.1f", bmi);
		}
	
	}

}
