import java.util.Scanner;

public class Zaehlen {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		int zaehler = 1;
		int eingabeZahl;

		System.out.print("Geben Sie bitte eine Zahl ein: ");
		eingabeZahl = tastatur.nextInt();

		heraufzaehlen(zaehler, eingabeZahl);
		herabzaehlen(zaehler, eingabeZahl);
		

	}
	public static void heraufzaehlen(int zaehler, int eingabeZahl) {
		while(zaehler <= eingabeZahl) {
			if(zaehler == eingabeZahl) {
				System.out.print(eingabeZahl);
			}
			else {
				System.out.print(zaehler + ", ");
			}
			zaehler++;
			
		}
		System.out.println();
	}
	public static void herabzaehlen(int zaehler, int eingabeZahl) {
		while(eingabeZahl >= zaehler) {
			if(eingabeZahl == zaehler) {
				System.out.print(zaehler);
			}
			else {
				System.out.print(eingabeZahl + ", ");
			}
			eingabeZahl--;
			
		}
	}

}
