import java.util.Scanner;

public class Zinseszins {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		
		System.out.print("Laufzeit (in Jahren) des Sparvertrags: ");
		int jahre = tastatur.nextInt();
		
		System.out.print("Wie viel Kapital (in Euro) m�chten Sie anlegen: ");
		double eKapital = tastatur.nextDouble();								// eKaptial steht f�r eingezahltes Kapital
		
		System.out.print("Zinssatz: ");
		double zinssatz = tastatur.nextDouble();
		
		int laufzeit = 1;
		double aKapital = eKapital;												// aKapital steht f�r augezahltes Kaptial
		
		while(laufzeit <= jahre) {
			aKapital = aKapital + ((aKapital * zinssatz) / 100);
			laufzeit++;
			
		}
		System.out.printf("Eingezahltes Kapital: %.2f Euro\n", eKapital);
		System.out.printf("Ausgezahltes Kapital: %.2f Euro", aKapital);
		
	}

}
