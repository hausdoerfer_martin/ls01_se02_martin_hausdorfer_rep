/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 8 ;
    
    // Anzahl der Sterne in unserer Milchstraße
    long anzahlSterne = 150000000000l ;
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3769000 ;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    short alterTage = 6614 ;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm = 200000 ;  
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
    double flaecheGroessteLand = 17.098242 ;
    
    // Wie groß ist das kleinste Land der Erde?
    
    float flaecheKleinsteLand = 0.44f ;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten );
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    
    System.out.println("Anzahl der Bewohner: " + bewohnerBerlin);
    
    System.out.println("Alter in Tage: " + alterTage);
    
    System.out.println("Gewicht in Kilogramm: " + gewichtKilogramm);
    
    System.out.println("Groesse der Erde in km�: " + flaecheGroessteLand);
    
    System.out.println("Kleinstes Land der Erde: " + flaecheKleinsteLand);
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

