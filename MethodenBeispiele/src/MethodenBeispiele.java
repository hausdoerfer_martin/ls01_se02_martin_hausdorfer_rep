import java.util.Scanner;
public class MethodenBeispiele {

	public static void main(String[] args) {
		
		
		Scanner myScanner = new Scanner (System.in);
		
	//	System.out.println("Geben sie bitte Ihren Namen ein: ");
	//	String name = myScanner.next();
		
		String vname = leseString("Geben sie bitte Ihren Vornamen ein:");
		String nname = leseString("Geben sie bitte Ihren Nachnamen ein:");
		int alter = leseInt("Geben sie bitte Ihr Alter ein:");
		sayHello(vname,nname,alter);
		

	}
	
	public static int leseInt(String text) {
		System.out.println(text);
		Scanner myScanner = new Scanner(System.in);
		int ergebnis = myScanner.nextInt();
		return ergebnis;
		
	}
	
	public static String leseString(String text) {
		System.out.println(text);
		Scanner myScanner = new Scanner(System.in);
		
		String str = myScanner.next();
		
		return str;
	}
	
	
	public static void sayHello(String vname, String nname, int alter) {
		System.out.println("hello " + vname + " " + nname+ " " + alter);	
	}

	
	public static void sayHello() {
		System.out.println("hello Martin");	
	}

}
