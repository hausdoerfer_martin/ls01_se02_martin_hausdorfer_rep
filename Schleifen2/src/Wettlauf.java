
public class Wettlauf {

	public static void main(String[] args) {
		int strecke = 1000;
		double sprinterA = 0;
		double sprinterB = 250;

		while((sprinterA <= strecke) && (sprinterB <= strecke)) {
			System.out.printf("sprinterA: %6.1f m sprinterB: %5.1f m\n", sprinterA, sprinterB);
			sprinterA += 9.7;
			sprinterB += 7;

		}
		if(sprinterA >= sprinterB) {
			System.out.println("\nSprinter A ist schneller im Ziel als Sprinter B.");
		}
		
		else {
			System.out.println("\nSprinter B ist schneller im Ziel als Sprinter A.");
		}
		
	}

}


