import java.util.Scanner;

public class Matrix {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		int zaehler = 0;

		System.out.print("Matrix\nBitte geben Sie eine Zahl zwischen 2 und 9 ein: ");
		int zahl1 = tastatur.nextInt();

		while (zaehler < 100) {

			int ersteStelle = zaehler / 10;
			int zweiteStelle = zaehler % 10;


			if (zaehler % zahl1 == 0) {
				System.out.printf("%4s", "*");
			}

			else if ( ersteStelle == zahl1 || zweiteStelle == zahl1) {
				System.out.printf("%4s", "*");
			}


			else if ( ersteStelle + zweiteStelle == zahl1) {
				System.out.printf("%4s", "*");
			}

			else {
				System.out.printf("%4d", zaehler);
			}
			zaehler++;

			if (zaehler % 10 == 0 && zaehler != 0) { 
				System.out.println();
			}

		}

	}

}







