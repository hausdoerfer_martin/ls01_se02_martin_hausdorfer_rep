import java.util.Scanner;

public class Schaltjahr {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Bitte geben sie Ihre Jahreszahl an: ");
		int jahreszahl = tastatur.nextInt();

		abfrage(jahreszahl);

	}
	public static boolean abfrage(int jahr) {
		if(((jahr <= 1582) && ((jahr % 4 == 0)) || ((jahr >= 1582) && ((jahr % 4 == 0) && (jahr % 100 != 0)) || (jahr % 400 == 0)))) {
			System.out.println("Das angegebene Jahr " + jahr + " ist ein Schaltjahr.");
			return true;
		}
		else{
			System.out.println("Das angegebene Jahr " + jahr + " ist kein Schaltjahr.");
			return false;
		}
	}

}


