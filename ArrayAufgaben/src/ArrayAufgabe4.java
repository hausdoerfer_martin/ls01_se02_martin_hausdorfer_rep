
public class ArrayAufgabe4 {

	public static void main(String[] args) {

		int[] lotto = {3, 7, 12, 18, 37, 42};

		System.out.print("[ ");

		for(int i = 0; i < lotto.length; i++) {
			System.out.print(lotto[i] + " ");
		}
		System.out.print("]\n");

		for(int i = 0; i <= 49; i++) {
			if(i == lotto[0] || i == lotto[1] || i == lotto[2] || i == lotto[3] || i == lotto[4] ||i == lotto[5]) {
				System.out.println("Die Zahl " + i + " ist in der Ziehung enthalten.");
			}
			else {
				System.out.println("Die Zahl " + i + " ist nicht in der Ziehung enthalten.");
			}
				
		}

	}

}
