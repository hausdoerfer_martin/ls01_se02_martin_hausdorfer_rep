import java.util.Scanner;

public class Monate {

	public static void main(String[] args) {

		monatAbfrage();

	}
	public static void monatAbfrage() {
		Scanner tastatur = new Scanner(System.in);
		System.out.printf("Bitte geben Sie den gewünschten Monat ein:");
		int monat = tastatur.nextInt();

		switch (monat) {
		case 1:
			System.out.println("Ihr gewünschter Monat ist Januar");
			break;
		case 2:
			System.out.println("Ihr gewünschter Monat ist Februar");
			break;
		case 3:
			System.out.println("Ihr gewünschter Monat ist März");
			break;
		case 4:
			System.out.println("Ihr gewünschter Monat ist April");
			break;
		case 5:
			System.out.println("Ihr gewünschter Monat ist Mai");
			break;
		case 6:
			System.out.println("Ihr gewünschter Monat ist Juni");
			break;
		case 7:
			System.out.println("Ihr gewünschter Monat ist Juli");
			break;
		case 8:
			System.out.println("Ihr gewünschter Monat ist August");
			break;
		case 9:
			System.out.println("Ihr gewünschter Monat ist September");
			break;
		case 10:
			System.out.println("Ihr gewünschter Monat ist Oktober");
			break;
		case 11:
			System.out.println("Ihr gewünschter Monat ist November");
			break;
		case 12:
			System.out.println("Ihr gewünschter Monat ist Dezember");
			break;
		default:
			System.out.printf("Fehler!\nBitte geben Sie einen Monat von 1 bis 12 ein!");
			break;
		}
	}

}
