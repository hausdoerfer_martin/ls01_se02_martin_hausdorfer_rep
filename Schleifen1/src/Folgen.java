
public class Folgen {

	public static void main(String[] args) {
		
		int startwertA;
		int endwertA = 12;
		int startwertB;
		int endwertB = 19;
		int startwertC;
		int endwertC = 98;
		int startwertD;
		int endwertD = 30;
		int startwertE;
		int endwertE = 16384;
		
		
		System.out.print("a) ");
		for(startwertA = 99; startwertA >= endwertA; startwertA -= 3) {
			System.out.print(startwertA + ", ");
		}
		System.out.print("9\n");
		/*------------------------------------------------------------------*/
		
		System.out.print("b) ");
		for(startwertB = 1; startwertB <= endwertB; startwertB++) {
			System.out.print(startwertB * startwertB + ", ");
		}
		System.out.print("400\n");
		/*------------------------------------------------------------------*/
		
		System.out.print("c) ");
		for(startwertC = 2; startwertC <= endwertC; startwertC += 2) {
			System.out.print(startwertC + ", ");
		}
		System.out.print("102\n");
		/*------------------------------------------------------------------*/
		
		System.out.print("d) ");
		for(startwertD = 2; startwertD <= endwertD; startwertD += 2) {
			
			System.out.print(startwertD * startwertD + ", ");
		}
		System.out.print("1024\n");
		/*------------------------------------------------------------------*/

		System.out.print("e) ");
		for(startwertE = 2; startwertE <= endwertE; startwertE += startwertE) {
			
			System.out.print(startwertE + ", ");
		}
		System.out.print("32768\n");
	}

}
