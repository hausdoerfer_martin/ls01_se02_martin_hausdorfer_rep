import java.util.Scanner;

public class SchleifenBeispiele {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		int eingabeZahl;
		int zaehler = 1;
		int sum = 0;
		
		System.out.print("Geben Sie bitte eine Zahl ein: ");
		eingabeZahl = tastatur.nextInt();

		while(zaehler <= eingabeZahl) {
			System.out.print(" = ");

			if(zaehler == eingabeZahl) {
				System.out.print(" = ");

			}
			else {
				System.out.print(" + ");
			}
			sum = sum + zaehler;

			zaehler++;
		}
		System.out.println(sum);

	}

}
