import java.util.Scanner;

public class Temperaturumrechnung {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		
		System.out.print("Bitte den Startwert in Celsius eingeben: ");
		double startwert = tastatur.nextDouble();
		
		System.out.print("Bitte den Endwert in Celsius eingeben: ");
		double endwert = tastatur.nextDouble();
		
		System.out.print("Bitte die Schrittweite in Grad Celsius eingeben: ");
		double schrittweite = tastatur.nextDouble();
		
		double cwert = startwert;
		double fwert = 0;
		
		while(cwert <= endwert) {
			fwert = (cwert * 1.8) + 32;
			
			System.out.printf("%.2f�C %10.2f�F\n", cwert, fwert);
			
			cwert = cwert + schrittweite;
		}
	}

}
