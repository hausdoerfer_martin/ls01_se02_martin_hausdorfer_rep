// Importieren einer ArrayList

import java.util.ArrayList;

public class Raumschiff {
	// Privates Deklarieren von Variablen (Sodass diese nicht standardmaeßig gesehen
	// werden können)

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();

	// Erstellen der Klasse Raumschiff mit den Standardattributen die ausgegeben
	// werden

	public Raumschiff() {
		this.photonentorpedoAnzahl = 0;
		this.energieversorgungInProzent = 0;
		this.schildeInProzent = 0;
		this.huelleInProzent = 0;
		this.lebenserhaltungssystemeInProzent = 0;
		this.androidenAnzahl = 0;
		this.schiffsname = null;

	}

//	Erstellen einer weiteren Raumschiffklasse welcher dann die Werte zugetragen bekommt

	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}

//	Setter und Getter fuer die Raumschiffe werden hier geschrieben

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

//	Hier wird die Methode torpedosAbschiessen erstellt, welche beim Abschuss eines Torpedos einen Torpedo abgezogen bekommt und eine Nachricht ausgibt
//	Sind die Torpedos leer dann wird eine andere Nachricht ausgegeben

	public void torpedosAbschiessen(Raumschiff angriffsziel) {
		if (photonentorpedoAnzahl >= 1) {
			System.out.println("\nPhotonentorpedo abgeschossen!");
			photonentorpedoAnzahl = photonentorpedoAnzahl - 1;
			treffer(angriffsziel);
		} else {
			System.out.println("-=*Click*=-\n");
		}

	}

//	Hier wird die Methode treffer erstellt, diese erfasst welches Ziel abgeschossen und getroffen wurde, gibt diese an den Broadcastkommunikator und gibt eine Nachricht aus in der steht, welches Ziel getroffen wurde

	public void treffer(Raumschiff angriffsziel) {
		broadcastKommunikator.add(angriffsziel.getSchiffsname() + " wurde getroffen!");
		System.out.println(angriffsziel.getSchiffsname() + " wurde getroffen!\n");
	}

//	Hier wird die Methode raumschiffstatusAbrufen erstellt, welche den Status der jeweiligen Raumschiffe in der Konsole ausgibt 

	public void raumschiffstatusAbrufen() {

		System.out.println("Photonentorpedoanzahl: " + getPhotonentorpedoAnzahl());
		System.out.println("Energieversorgung in Prozent: " + getEnergieversorgungInProzent());
		System.out.println("Schilde in Prozent: " + getSchildeInProzent());
		System.out.println("Huelle in Prozent: " + getHuelleInProzent());
		System.out.println("Lebenserhaltungssysteme in Prozent: " + getLebenserhaltungssystemeInProzent());
		System.out.println("Androidenanzahl: " + getAndroidenAnzahl());
		System.out.println("Schiffsname: " + getSchiffsname());

	}

//	Hier passiert das selbe wie bei den Torpedos nur mit dem Unterschied, dass es Phaserkanonen sind und nicht Photonentorpedos
//	Diese werden von der energieversorgung angetrieben und deswegen wird bei einem Schuss die energieversorgung - 50 gerechnet

	public void phaserkanonenAbschiessen(Raumschiff angriffsziel) {
		if (energieversorgungInProzent >= 50) {
			System.out.println("\nPhaserkanonen abgeschossen!");
			energieversorgungInProzent = energieversorgungInProzent - 50;
			treffer(angriffsziel);
		} else {
			System.out.println("-=*Click*=-\n");
		}
	}

}
