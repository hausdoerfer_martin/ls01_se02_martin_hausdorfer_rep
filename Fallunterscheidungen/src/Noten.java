import java.util.Scanner;

public class Noten {

	public static void main(String[] args) {

		notenausgabe();

	}
	public static void notenausgabe() {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Bitte geben sie Ihre Note ein:");
		int note = tastatur.nextInt();

		switch (note) {
		case 1:
			System.out.println("Ihre Note ist Sehr Gut");
			break;
		case 2:
			System.out.println("Ihre Note ist Gut");
			break;
		case 3:
			System.out.println("Ihre Note ist Befriedigend");
			break;
		case 4:
			System.out.println("Ihre Note ist Ausreichend");
			break;
		case 5:
			System.out.println("Ihre Note ist Mangelhaft");
			break;
		case 6:
			System.out.println("Ihre Note ist Ungenügend");
			break;
		default:
			System.out.printf("Fehler!\nBitte geben Sie eine Note von 1 bis 6 ein!9");
			break;

		}
	}

}
