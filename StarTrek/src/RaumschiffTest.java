// Importieren des Scanners

import java.util.Scanner;

public class RaumschiffTest {

	public static void main(String[] args) {
//		Einfuegen des Scanners: tastatur

		Scanner tastatur = new Scanner(System.in);

//		Hinzufuegen der 3 Raumschiffe

		Raumschiff r1 = new Raumschiff();
		Raumschiff r2 = new Raumschiff();
		Raumschiff r3 = new Raumschiff();

//		Erstellen einer Endlosschleife mit einer Switch-Case Methode darin, in der Methoden abgerufen werden

		int auswahl;
		do {
			auswahl = menu();
			switch (auswahl) {
			case 1:
				raumschiffdatenFuellen(r1, r2, r3);
				break;
			case 2:
				raumschiffstatusAbrufen(r1, r2, r3);
				break;
			case 3:
				raumschiffladungAbrufen();
				break;
			case 4:
				torpedoTest(r1, r2, r3);
				break;
			case 5:
				phaserkanonenTest(r1, r2, r3);
				break;
			case 6:
				broadcastKommunikatorTest(r1, r2, r3);
				break;
			default:
				System.err.println("\nFalsche Eingabe.\n");
			}
		} while (true);

	}

//	Erstellen der Methode menu, damit Fragen in der Konsole angezeigt werden mit der man wei� mit welcher Tastae man die jeweiligen Methoden aufrufen kann

	public static int menu() {

		int selection;
		Scanner input = new Scanner(System.in);

		/***************************************************/

		System.out.println("------------------------------------\n");
		System.out.println("Waehlen Sie die gew�nschte Option: ");
		System.out.println("1 - Raumschiffdaten auff�llen");
		System.out.println("2 - Raumschiffsstatus abrufen");
		System.out.println("3 - Raumschiffladungen abrufen");
		System.out.println("4 - Photonentorpedos abschiessen");
		System.out.println("5 - Phaserkanonen abschiessen");
		System.out.println("6 - Broadcast Kommunikator abrufen");

		System.out.print("\nEingabe:  ");
		selection = input.nextInt();
		return selection; // Rueckgabe des Wertes selection
	}

//	Erstellen der Methode raumschiffstatusAbrufen, in der die Statusinformationen der 3 Raumschiffe aufgerufen werden

	public static void raumschiffstatusAbrufen(Raumschiff r1, Raumschiff r2, Raumschiff r3) {
		r1.raumschiffstatusAbrufen();
		r2.raumschiffstatusAbrufen();
		r3.raumschiffstatusAbrufen();
	}

//	Erstellen der Methoode raumschiffladungAbrufen, in der die Ladungen der jeweiligen Raumschiffe angezeigt werden

	public static void raumschiffladungAbrufen() {

		System.out.println("\n-------------------------------------------------");
		System.out.println("\nLadung von Raumschiff 1 ");
		Ladung l1 = new Ladung("Ferengi Schneckensaft", 200);
		System.out.println("\nLadung 1:");
		System.out.print(l1.getBezeichnung() + ":");
		System.out.println(" " + l1.getMenge());

		Ladung l5 = new Ladung("Bat'leth Klingonen Schwert", 200);
		System.out.println("\nLadung 2:");
		System.out.print(l5.getBezeichnung() + ":");
		System.out.println(" " + l5.getMenge());

		System.out.println("\n-------------------------------------------------");

		System.out.println("\nLadung von Raumschiff 2 ");
		System.out.println("\nLadung 1:");
		Ladung l2 = new Ladung("Borg-Schrott", 5);
		System.out.print(l2.getBezeichnung() + ":");
		System.out.println(" " + l2.getMenge());

		System.out.println("\nLadung 2:");
		Ladung l3 = new Ladung("Rote Materie", 2);
		System.out.print(l3.getBezeichnung() + ":");
		System.out.println(" " + l3.getMenge());

		System.out.println("\nLadung 3:");
		Ladung l6 = new Ladung("Plasma-Waffe", 50);
		System.out.print(l6.getBezeichnung() + ":");
		System.out.println(" " + l6.getMenge());

		System.out.println("\n-------------------------------------------------");

		System.out.println("\nLadung von Raumschiff 3 ");
		System.out.println("\nLadung 1:");
		Ladung l4 = new Ladung("Forschungssonde", 35);
		System.out.print(l4.getBezeichnung() + ":");
		System.out.println(" " + l4.getMenge());

		System.out.println("\nLadung 2:");
		Ladung l7 = new Ladung("Photonentorpedo", 3);
		System.out.print(l7.getBezeichnung() + ":");
		System.out.println(" " + l7.getMenge());

		System.out.println("\n-------------------------------------------------");
	}

//	Erstellen der Methode phaserkanonenTest, in der die 3 Raumschiffe ein ausgewaehltes Ziel mit den Phaserkanonen angreifen
//	Das Ziel kann ver�ndert werden, wenn man den Wert in den Klammern aendert

	public static void phaserkanonenTest(Raumschiff r1, Raumschiff r2, Raumschiff r3) {
		r1.phaserkanonenAbschiessen(r2);
		r2.phaserkanonenAbschiessen(r2);
		r3.phaserkanonenAbschiessen(r1);
	}

//	Erstellen der Methode torpedoTest, in der die 3 Raumschiffe ein ausgewaehltes Ziel mit den Torpedos angreifen
//	Das Ziel kann ver�ndert werden, wenn man den Wert in den Klammern aendert

	public static void torpedoTest(Raumschiff r1, Raumschiff r2, Raumschiff r3) {
		r1.torpedosAbschiessen(r3);
		r2.torpedosAbschiessen(r3);
		r3.torpedosAbschiessen(r1);
	}

//	Erstellen der Methode raumschiffdatenFuellen, welche am Anfang des Spiels aufgerufen werden muss, damit alle Raumschiffe ihre Startdaten zugewiesen bekommen
//	Die Daten der jeweiligen Raumschiffe k�nnen ge�ndert werden, indem man die Werte in den Klammern veraendert

	public static void raumschiffdatenFuellen(Raumschiff r1, Raumschiff r2, Raumschiff r3) {
		r1.setPhotonentorpedoAnzahl(1);
		r1.setEnergieversorgungInProzent(100);
		r1.setSchildeInProzent(100);
		r1.setHuelleInProzent(100);
		r1.setLebenserhaltungssystemeInProzent(100);
		r1.setAndroidenAnzahl(2);
		r1.setSchiffsname("IKS Hegh'ta");

		r2.setPhotonentorpedoAnzahl(2);
		r2.setEnergieversorgungInProzent(100);
		r2.setSchildeInProzent(100);
		r2.setHuelleInProzent(100);
		r2.setLebenserhaltungssystemeInProzent(100);
		r2.setAndroidenAnzahl(2);
		r2.setSchiffsname("IRW Khazara");

		r3.setPhotonentorpedoAnzahl(0);
		r3.setEnergieversorgungInProzent(80);
		r3.setSchildeInProzent(80);
		r3.setHuelleInProzent(50);
		r3.setLebenserhaltungssystemeInProzent(100);
		r3.setAndroidenAnzahl(5);
		r3.setSchiffsname("Ni'Var");

	}

//	Erstellen der Methode broadcastKommunikatorTest, welche alles ausgibt, was die jeweiligen Raumschiffe als letztes in den Broadcastkommunikator geschrieben haben

	public static void broadcastKommunikatorTest(Raumschiff r1, Raumschiff r2, Raumschiff r3) {
		System.out.println(r1.getBroadcastKommunikator());
		System.out.println(r2.getBroadcastKommunikator());
		System.out.println(r3.getBroadcastKommunikator());

	}
}
