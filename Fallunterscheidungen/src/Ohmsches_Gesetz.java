import java.util.Scanner;

public class Ohmsches_Gesetz {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie in Gro�buchstaben ein, welche Einheit des Ohmschen Gesetz berechnet werden soll:");
		String ohmschesZeichen = tastatur.next();
		
		rechnung(ohmschesZeichen);
		
	}
	public static void rechnung(String ohmschesZeichen) {
		Scanner tastatur = new Scanner(System.in);

		if(ohmschesZeichen.equals("U")) {
			System.out.println("Bitte geben Sie die gegebene Zahl f�r R ein:");
			double zahlR = tastatur.nextDouble();
			
			System.out.println("Bitte geben Sie die gegebene Zahl f�r I ein:");
			double zahlI = tastatur.nextDouble();
			
			double ergebnis = zahlR * zahlI;
			
			System.out.printf("Das Ergebnis der Spannung (U) ist: %.2fV", ergebnis);
		}
		if(ohmschesZeichen.equals("R")) {
			System.out.println("Bitte geben Sie die gegebene Zahl f�r U ein:");
			double zahlU = tastatur.nextDouble();
			
			System.out.println("Bitte geben Sie die gegebene Zahl f�r I ein:");
			double zahlI = tastatur.nextDouble();
			
			double ergebnis = zahlU / zahlI;
			
			System.out.printf("Das Ergebnis des Widerstandes (R) ist: %.2f Ohm", ergebnis);
		}
		if(ohmschesZeichen.equals("I")) {
			System.out.println("Bitte geben Sie die gegebene Zahl f�r U ein:");
			double zahlU = tastatur.nextDouble();
			
			System.out.println("Bitte geben Sie die gegebene Zahl f�r R ein:");
			double zahlR = tastatur.nextDouble();
			
			double ergebnis = zahlU / zahlR;
			
			System.out.printf("Das Ergebnis der Stromst�rke (I) ist: %.2fA", ergebnis);
		}
		else {
			System.out.printf("Fehler!\nBitte geben Sie ein g�ltiges Zeichen ein!");
		}
	}

}
