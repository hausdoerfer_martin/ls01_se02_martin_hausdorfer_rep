
public class Einmaleins {

	public static void main(String[] args) {

		int startwert;
		int endwert;

		for(startwert = 1, endwert = 10; startwert <= endwert; startwert += 1) {
			System.out.print( " | " + startwert);
		}
		System.out.print(" |\n");

		for(startwert = 2, endwert = 20; startwert <= endwert; startwert += 2) {
			System.out.print( " | " + startwert);
		}
		System.out.print(" |\n");

		for(startwert = 3, endwert = 30; startwert <= endwert; startwert += 3) {
			System.out.print( " | " + startwert);
		}
		System.out.print(" |\n");

		for(startwert = 4, endwert = 40; startwert <= endwert; startwert += 4) {
			System.out.print( " | " + startwert);
		}
		System.out.print(" |\n");

		for(startwert = 5, endwert = 50; startwert <= endwert; startwert += 5) {
			System.out.print( " | " + startwert);
		}
		System.out.print(" |\n");

		for(startwert = 6, endwert = 60; startwert <= endwert; startwert += 6) {
			System.out.print( " | " + startwert);
		}
		System.out.print(" |\n");

		for(startwert = 7, endwert = 70; startwert <= endwert; startwert += 7) {
			System.out.print( " | " + startwert);
		}
		System.out.print(" |\n");

		for(startwert = 8, endwert = 80; startwert <= endwert; startwert += 8) {
			System.out.print( " | " + startwert);
		}
		System.out.print(" |\n");

		for(startwert = 9, endwert = 90; startwert <= endwert; startwert += 9) {
			System.out.print( " | " + startwert);
		}
		System.out.print(" |\n");

		for(startwert = 10, endwert = 100; startwert <= endwert; startwert += 10) {
			System.out.print( " | " + startwert);
		}

	}

}
