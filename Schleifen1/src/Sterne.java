import java.util.Scanner;

public class Sterne {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		int startwert;
		int zeilen;
		int endwert;

		System.out.println("Bitte geben Sie die Anzahl an Zeilen an:");

		for(startwert = 1, zeilen = tastatur.nextInt(); startwert <= zeilen; startwert++) { 
			for(endwert = 1; endwert <= startwert; endwert++) {
				System.out.print("*");
			}
			System.out.println("");

		}

	}

}
