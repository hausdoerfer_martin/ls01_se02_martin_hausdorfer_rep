import java.util.Scanner;

public class Million {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		boolean standardausgabe = true;
		
		while(standardausgabe == true) {
			System.out.print("Wie viel Kapital (in Euro) m�chten Sie anlegen: "); 
			double kapitaleinlage = tastatur.nextDouble();

			System.out.print("Wie viel % betr�gt Ihr Zinssatz: ");
			double zinssatz = tastatur.nextDouble();

			int laufzeit = 0;
			double kapital = kapitaleinlage;

			while(kapital <= 1000000) {
				kapital = kapital + ((kapital * zinssatz) / 100);
				laufzeit++;

			}
			
			System.out.println("\nSie ben�tigen " + laufzeit + " Jahre um mit Ihrer Einlage Million�r zu werden.\n");

			System.out.print("Wollen sie noch einmal mit anderen Werten rechnen? j / n: ");
			String nochmal =  tastatur.next();

			switch(nochmal) {
			case "j":
				standardausgabe = true;
				System.out.println();
				break;

			case "n":
				standardausgabe = false;
				System.out.println("\nWir w�nschen Ihnen einen sch�nen Tag!");
				break;
			default:
				System.out.println("\nFalsche Eingabe!\nDas System startet nun von vorne!\n");
				break;
			}
			
		}

	}

}
