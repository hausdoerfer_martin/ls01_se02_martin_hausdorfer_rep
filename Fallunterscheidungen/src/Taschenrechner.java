import java.util.Scanner;

public class Taschenrechner {

	public static void main(String[] args) {
		
		rechner();
		
	}
	public static void rechner() {
		Scanner tastatur = new Scanner(System.in);
		double ergebnis;
		
		System.out.println("Bitte geben Sie die erste gew�nschte Zahl ein:");
		double zahl1 = tastatur.nextDouble();
		
		System.out.println("Bitte geben Sie die zweite gew�nschte Zahl ein:");
		double zahl2 = tastatur.nextDouble();
		
		System.out.println("Bitte geben Sie mit +,-,* und / ein, wie diese Zahlen ausgerechnet werden sollen:");
		String symbol = tastatur.next();
		
		switch (symbol) {
		case "+":
			ergebnis = zahl1 + zahl2;
			System.out.printf("Ihr ergebnis lautet %.2f", ergebnis);
			break;
		case "-":
			ergebnis = zahl1 - zahl2;
			System.out.printf("Ihr ergebnis lautet %.2f", ergebnis);
			break;
		case "*":
			ergebnis = zahl1 * zahl2;
			System.out.printf("Ihr ergebnis lautet %.2f", ergebnis);
			break;
		case "/":
			ergebnis = zahl1 / zahl2;
			System.out.printf("Ihr ergebnis lautet %.2f", ergebnis);
			break;
			default:
				System.out.printf("Fehler!\nBitte geben Sie ein g�ltiges Rechensymbol ein!");
				break;
		}
	}

}
