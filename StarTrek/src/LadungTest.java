
public class LadungTest {

	public static void main(String[] args) {
		Ladung l1 = new Ladung();
		System.out.println("Unbekannte Ladung: ");
		System.out.println(l1.getBezeichnung());
		System.out.println(l1.getMenge());
		
		System.out.println("\nLadung 1:");
		l1.setBezeichnung("Ferengi Schneckensaft");
		System.out.println(l1.getBezeichnung());
		l1.setMenge(200);
		System.out.println(l1.getMenge());
		
		System.out.println("\nLadung 2:");
		Ladung l2 = new Ladung("Borg-Schrott", 5);
		System.out.println(l2.getBezeichnung());
		System.out.println(l2.getMenge());
		
		System.out.println("\nLadung 3:");
		Ladung l3 = new Ladung("Rote Materie", 2);
		System.out.println(l3.getBezeichnung());
		System.out.println(l3.getMenge());
		
		System.out.println("\nLadung 4:");
		Ladung l4 = new Ladung("Forschungssonde", 35);
		System.out.println(l4.getBezeichnung());
		System.out.println(l4.getMenge());
		
		System.out.println("\nLadung 5:");
		Ladung l5 = new Ladung("Bat'leth Klingonen Schwert", 200);
		System.out.println(l5.getBezeichnung());
		System.out.println(l5.getMenge());
		
		System.out.println("\nLadung 6:");
		Ladung l6 = new Ladung("Plasma-Waffe", 50);
		System.out.println(l6.getBezeichnung());
		System.out.println(l6.getMenge());
		
		System.out.println("\nLadung 7:");
		Ladung l7 = new Ladung("Photonentorpedo", 3);
		System.out.println(l7.getBezeichnung());
		System.out.println(l7.getMenge());
	}

}
