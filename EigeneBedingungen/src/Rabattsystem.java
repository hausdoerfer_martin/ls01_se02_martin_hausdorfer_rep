import java.util.Scanner;

public class Rabattsystem {

	public static void main(String[] args) {
		double rabatt10 = 0.10;
		double rabatt15 = 0.15;
		double rabatt20 = 0.20;
		double mwst = 0.19;

		bestellung(rabatt10, rabatt15, rabatt20, mwst);
	}
	public static void bestellung(double rabatt10, double rabatt15, double rabatt20, double mwst) {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Bitte geben sie den Bestellwert ein: ");
		double bestellwert = tastatur.nextDouble();

		if(bestellwert > 0 && bestellwert < 100 ) {
			double rabattpreis10 = bestellwert - (bestellwert * mwst) - (rabatt10);
			System.out.println("Der Bestellwert (zzg. MwSt) mit dem Rabatt von 10% betr�gt: " + rabattpreis10);

		}
		if(bestellwert > 100 && bestellwert < 500) {
			double rabattpreis15 = bestellwert - (bestellwert * mwst) - (rabatt15);
			System.out.println("Der Bestellwert (zzg. MwSt) mit dem Rabatt von 15% betr�gt: " + rabattpreis15);

		}
		if(bestellwert > 500) {
			double rabattpreis20 = bestellwert - (bestellwert * mwst) - (rabatt20);
			System.out.println("Der Bestellwert (zzg. MwSt) mit dem Rabatt von 20% betr�gt: " + rabattpreis20);

		}

	}
}