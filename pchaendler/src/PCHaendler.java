import java.util.Scanner;

public class PCHaendler {
	
	public static String liesString(String text) {
		System.out.println(text);
		Scanner myScanner = new Scanner(System.in);
		String ergebnis = myScanner.next();
		return ergebnis;
		
	}
	
	public static int liesInt(String text) {
		System.out.println(text);
		Scanner myScanner = new Scanner(System.in);
		int ergebnis = myScanner.nextInt();
		return ergebnis;
		
	}
	
	public static double liesDouble(String text) {
		System.out.println(text);
		Scanner myScanner = new Scanner(System.in);
		double ergebnis = myScanner.nextDouble();
		return ergebnis;
	}
	
	
	public static double berechneGesamtnettopreis(int anzahl, double preis) {
		double nettogesamtpreis = anzahl * preis;
		return nettogesamtpreis;
				
	}
	
	public static double berechneGesamtnettopreis(double nettogesamtpreis, double mwst) {
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst/100 );
		return bruttogesamtpreis;
		
	}
	
	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		
		
	}


	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		String artikel = liesString("Was m�chten Sie bestellen?");
		
		int anzahl = liesInt("Geben Sie die Anzahl ein:");

		double preis = liesDouble("Geben Sie den Nettopreis ein:");

		
		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");

		// Verarbeiten
		
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtnettopreis(nettogesamtpreis, mwst);

		// Ausgeben

		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");

	}

}
